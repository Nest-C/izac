import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class editname extends StatefulWidget {
  const editname({super.key});

  @override
  State<editname> createState() => _editnameState();
}

class _editnameState extends State<editname> {
  Future<void> updateName(String newName) async {
    final user = FirebaseAuth.instance.currentUser;
    var FirebaseFirestore;
    final userRef =
        FirebaseFirestore.instance.collection('users').doc(user!.uid);
    await userRef.update({'name': newName});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(""),
      ),
      body: ElevatedButton(
        onPressed: () async {
          var controller;
          final newName = await showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: const Text('Edit Name'),
              content: TextField(
                autofocus: true,
                decoration: const InputDecoration(
                  hintText: 'Enter your new name',
                ),
                onSubmitted: (newName) => Navigator.of(context).pop(newName),
              ),
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: const Text('CANCEL'),
                ),
                ElevatedButton(
                  onPressed: () => Navigator.of(context).pop(controller.text),
                  child: const Text('SAVE'),
                ),
              ],
            ),
          );
          if (newName != null && newName.isNotEmpty) {
            await updateName(newName);
            ScaffoldMessenger.of(context).showSnackBar(
              const SnackBar(content: Text('Name updated')),
            );
          }
        },
        child: const Text('Edit Name'),
      ),
    );
  }
}
