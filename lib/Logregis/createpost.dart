import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:project/Logregis/post.dart';

class CreatePostScreen extends StatelessWidget {
  final formkey = GlobalKey<FormState>();
  Post myPost = Post(madepost: '');
  //เตรียม firebase
  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  final CollectionReference _post =
      FirebaseFirestore.instance.collection("Data");

  CreatePostScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
              appBar: AppBar(
                title: const Text("Error"),
              ),
              body: Center(
                child: Text("${snapshot.error}"),
              ),
            );
          }
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              appBar: AppBar(
                title: const Text('Create Post'),
              ),
              body: Padding(
                padding: const EdgeInsets.all(16),
                child: SingleChildScrollView(
                    child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextFormField(
                      validator:
                          RequiredValidator(errorText: "กรุณากรอกข้อความ"),
                      maxLines: 5,
                      decoration: const InputDecoration(
                        hintText: 'What\'s on your mind?',
                        border: OutlineInputBorder(),
                      ),
                      onSaved: (String? madepost) {
                        myPost.madepost = madepost!;
                      },
                    ),
                    const SizedBox(height: 8),
                    const Text('Add your Post'),
                    IconButton(
                      icon: const Icon(Icons.post_add_sharp),
                      onPressed: () async {
                        if (formkey.currentState!.validate()) {
                          formkey.currentState?.save();
                          _post.add({
                            "madepost": myPost.madepost,
                          });
                          formkey.currentState?.reset();
                        }
                      },
                    ),
                  ],
                )),
              ),
            );
          }
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        });
  }
}
