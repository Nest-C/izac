import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'createpost.dart';

class MySearchPage extends StatefulWidget {
  const MySearchPage({super.key});

  @override
  _MySearchPageState createState() => _MySearchPageState();
}

class _MySearchPageState extends State<MySearchPage> {
  final searchController = TextEditingController();
  List<String> searchHistory = [];

  @override
  void initState() {
    super.initState();
    _loadSearchHistory();
  }

  void _loadSearchHistory() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      searchHistory = prefs.getStringList('search History') ?? [];
    });
  }

  void _saveSearchHistory(String query) async {
    final prefs = await SharedPreferences.getInstance();
    if (!searchHistory.contains(query)) {
      searchHistory.add(query);
      prefs.setStringList('search History', searchHistory);
    }
  }

  void _clearSearchHistory() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      searchHistory.clear();
      prefs.remove('search History');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Score'),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CreatePostScreen();
          }));
        },
        child: const Icon(Icons.add),
      ),
      body: Column(
        children: [
          TextField(
            controller: searchController,
            onSubmitted: (value) {
              _saveSearchHistory(value);
            },
            decoration: InputDecoration(
              hintText: 'Search',
              prefixIcon: const Icon(Icons.search),
              suffixIcon: IconButton(
                icon: const Icon(Icons.clear),
                onPressed: () => searchController.clear(),
              ),
            ),
          ),
          if (searchHistory.isNotEmpty)
            Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(' History'),
                    IconButton(
                      icon: const Icon(Icons.clear_all),
                      onPressed: _clearSearchHistory,
                    ),
                  ],
                ),
                ListView.builder(
                  shrinkWrap: true,
                  itemCount: searchHistory.length,
                  itemBuilder: (context, index) {
                    return ListTile(
                      title: Text(searchHistory[index]),
                      onTap: () {
                        searchController.text = searchHistory[index];
                      },
                    );
                  },
                ),
              ],
            ),
        ],
      ),
    );
  }
}
