import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:project/Logregis/login.dart';
import 'package:project/model/profile.dart';

class setting extends StatefulWidget {
  const setting({super.key});

  @override
  State<setting> createState() => _settingState();
}

class _settingState extends State<setting> {
  final auth = FirebaseAuth.instance;
  bool _editing = false;
  String _name = '';
  final formKey = GlobalKey<FormState>();
  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  Future<void> updateName(String newName) async {
    final user = FirebaseAuth.instance.currentUser;
    var FirebaseFirestore;
    final userRef =
        FirebaseFirestore.instance.collection('users').doc(user!.uid);
    await userRef.update({'name': newName});
  }

  Profile profile = Profile(
    email: '',
    password: '',
    name: '',
  );

  void _toggleEditMode() {
    setState(() {
      _editing = !_editing;
    });
  }

  void _saveChanges() {
    String newName = _nameController.text.trim();
    if (newName.isNotEmpty) {
      setState(() {
        _name = newName;
        _editing = false;
      });
    }
  }

  final _nameController = TextEditingController(text: '');
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: firebase,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          if (!snapshot.hasData) {
            return const CircularProgressIndicator();
          }
          final name = snapshot.data!;
          return Text('Hello, $name!');
        }
        return Scaffold(
          appBar: AppBar(
            title: const Text("setting"),
          ),
          body: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Center(
              child: Column(
                children: [
                  Text(
                    auth.currentUser!.email!,
                    style: const TextStyle(fontSize: 20),
                  ),
                  TextField(
                    enabled: _editing,
                    controller: _nameController,
                    decoration: const InputDecoration(
                      labelText: 'Name',
                    ),
                  ),
                  if (_editing)
                    ElevatedButton(
                      child: const Text('Save', style: TextStyle(fontSize: 10)),
                      onPressed: () async {
                        formKey.currentState?.save();
                        _saveChanges();
                      },
                    )
                  else
                    ElevatedButton(
                      onPressed: _toggleEditMode,
                      child: const Text('Edit'),
                    ),
                  ElevatedButton(
                    child: const Text("logout"),
                    onPressed: () {
                      auth.signOut().then((value) {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                          return const login();
                        }));
                      });
                    },
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
