import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:project/Logregis/login.dart';
import 'package:project/model/profile.dart';

class ResetPasswordPage extends StatefulWidget {
  const ResetPasswordPage({super.key});

  @override
  State<ResetPasswordPage> createState() => _ResetPasswordPageState();
}

class _ResetPasswordPageState extends State<ResetPasswordPage> {
  final formkey = GlobalKey<FormState>();
  Profile profile = Profile(
    email: '',
    password: '',
    name: '',
  );

  final Future<FirebaseApp> firebase = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      key: formkey,
      appBar: AppBar(
        title: const Text("Reset password", style: TextStyle(color: Colors.white)),
        iconTheme: const IconThemeData(
          color: Colors.white,
        ),
      ),
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/background.jpg'),
            fit: BoxFit.fitHeight,
          ),
        ),
        child: Container(
          margin: const EdgeInsets.fromLTRB(50, 300, 50, 100),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16),
              gradient: const LinearGradient(colors: [
                Colors.green,
                Color.fromARGB(255, 208, 255, 0),
              ])),
          padding: const EdgeInsets.all(10),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(
              10,
              2,
              10,
              0,
            ),
            child: Form(
                child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.all(10.0),
                    margin: const EdgeInsets.only(top: 12),
                    decoration: BoxDecoration(
                        color: Colors.green[50],
                        borderRadius: BorderRadius.circular(16)),
                    child: const TextField(
                      decoration: InputDecoration.collapsed(hintText: "Email"),
                      keyboardType: TextInputType.emailAddress,
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: InkWell(
                      child: Container(
                        constraints: const BoxConstraints.expand(height: 50),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(16),
                            color: Colors.green[200]),
                        margin: const EdgeInsets.only(top: 16),
                        padding: const EdgeInsets.all(12),
                        child: const Text("send email",
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(fontSize: 18, color: Colors.white)),
                      ),
                      onTap: () => const login(),
                    ),
                  ),
                  SizedBox(
                    width: double.infinity,
                    child: ElevatedButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                          return const login();
                        }));
                      },
                      style: ElevatedButton.styleFrom(
                          backgroundColor: const Color.fromARGB(255, 2, 136, 13)),
                      child: const Text("ย้อนกลับ"),
                    ),
                  ),
                ],
              ),
            )),
          ),
        ),
      ),
    );
  }
}
