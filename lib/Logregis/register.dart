import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:project/Logregis/login.dart';
import 'package:project/model/profile.dart';

class Register extends StatefulWidget {
  const Register({super.key});

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final formKey = GlobalKey<FormState>();
  Profile profile = Profile(
    email: '',
    password: '',
    name: '',
  );

  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
                appBar: AppBar(
                  title: const Text("Error"),
                ),
                body: Center(
                  child: Text("${snapshot.error}"),
                ));
          }
          //กรอบ
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              appBar: AppBar(
                title: const Text(
                  "สร้างบัญชีผู้ใช้",
                  style: TextStyle(color: Colors.white),
                ),
              ),
              body: Container(
                color: Colors.green[50],
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      gradient: const LinearGradient(
                          colors: [Colors.green, Colors.yellow])),
                  margin: const EdgeInsets.all(32),
                  padding: const EdgeInsets.fromLTRB(20, 50, 10, 0),
                  child: Form(
                    key: formKey,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              padding: const EdgeInsets.all(10.0),
                              margin: const EdgeInsets.only(top: 12),
                              decoration: BoxDecoration(
                                  color: Colors.green[50],
                                  borderRadius: BorderRadius.circular(16)),
                              child: TextField(
                                decoration: const InputDecoration.collapsed(
                                    hintText: "Email"),
                                keyboardType: TextInputType.emailAddress,
                                onChanged: (String? email) {
                                  profile.email = email!;

                                  MultiValidator([
                                    RequiredValidator(
                                        errorText: "กรุณากรอกอีเมล"),
                                    EmailValidator(
                                        errorText: "รูปแบบอีเมลไม่ถูกต้อง")
                                  ]);
                                },
                              )),
                          //กรอกพาสเวิร์ด

                          Container(
                              padding: const EdgeInsets.all(10.0),
                              margin: const EdgeInsets.only(top: 12),
                              decoration: BoxDecoration(
                                  color: Colors.green[50],
                                  borderRadius: BorderRadius.circular(16)),
                              child: TextField(
                                decoration: const InputDecoration.collapsed(
                                    hintText: "Password"),
                                obscureText: true,
                                onChanged: (String? password) {
                                  profile.password = password!;
                                  RequiredValidator(
                                      errorText: "กรุณากรอกพาสเวิร์ด");
                                },
                              )),
                          const SizedBox(
                            height: 5,
                          ),
                          //ปุ่มลงทะเบียน
                          SizedBox(
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () async {
                                  if (formKey.currentState!.validate()) {
                                    formKey.currentState?.save();
                                    try {
                                      await FirebaseAuth.instance
                                          .createUserWithEmailAndPassword(
                                              email: profile.email,
                                              password: profile.password);
                                      formKey.currentState?.reset();
                                      Fluttertoast.showToast(
                                          msg: "สร้างบัญชีผู้ใช้เรียบร้อยแล้ว",
                                          gravity: ToastGravity.CENTER);
                                      Navigator.pushReplacement(context,
                                          MaterialPageRoute(builder: (context) {
                                        return const login();
                                      }));
                                    } on FirebaseAuthException catch (e) {
                                      print(e.code);
                                      String? message;
                                      if (e.code == 'email-already-in-use') {
                                        message = "มีอีเมลนี้ในระบบแล้ว";
                                      } else if (e.code == 'weak=password') {
                                        message =
                                            "รหัสผ่านต้องมีความยาว 6 ตัวอักษร";
                                      } else {
                                        message = e.message;
                                      }
                                      Fluttertoast.showToast(
                                          msg: "$message",
                                          gravity: ToastGravity.CENTER);
                                    }
                                  }
                                },
                                style: ElevatedButton.styleFrom(
                                    backgroundColor:
                                        const Color.fromARGB(255, 2, 136, 13)),
                                child: const Text("ลงทะเบียน"),
                              )),
                          SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: () {
                                Navigator.pushReplacement(context,
                                    MaterialPageRoute(builder: (context) {
                                  return const login();
                                }));
                              },
                              style: ElevatedButton.styleFrom(
                                  backgroundColor:
                                      const Color.fromARGB(255, 2, 136, 13)),
                              child: const Text("ย้อนกลับ"),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        });
  }
}
