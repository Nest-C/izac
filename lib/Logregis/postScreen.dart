import 'createpost.dart';
import 'package:flutter/material.dart';

class float extends StatelessWidget {
  const float({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('สร้างกระทู้'),
        actions: const [],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          Navigator.push(context, MaterialPageRoute(builder: (context) {
            return CreatePostScreen();
          }));
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
