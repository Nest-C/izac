import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:project/Logregis/editname.dart';
import 'package:project/Logregis/postScreen.dart';
import 'package:project/Logregis/ScoreScreen.dart';
import 'package:project/Logregis/settingScreen.dart';

class home extends StatelessWidget {
  final auth = FirebaseAuth.instance;

  home({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      child: Scaffold(
        body: TabBarView(
          children: [
            const MySearchPage(),
            const editname(),
            const float(),
            const setting(),
            Container(),
          ],
        ),
        backgroundColor: Colors.blue,
        bottomNavigationBar: const TabBar(
          tabs: [
            Tab(
              icon: Icon(Icons.home),
            ),
            Tab(
              icon: Icon(Icons.article),
            ),
            Tab(
              icon: Icon(Icons.person),
            ),
            Tab(
              icon: Icon(Icons.settings),
            ),
            Tab(
              icon: Icon(Icons.newspaper),
            )
          ],
        ),
      ),
    );
  }
}
