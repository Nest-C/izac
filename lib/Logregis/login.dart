import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:project/Logregis/Home.dart';
import 'package:project/Logregis/register.dart';
import 'package:project/Logregis/resetpassword.dart';
import '../model/profile.dart';

class login extends StatefulWidget {
  const login({super.key});

  @override
  _loginState createState() => _loginState();
}

class _loginState extends State<login> {
  final formKey = GlobalKey<FormState>();
  Profile profile = Profile(
    email: '',
    password: '',
    name: '',
  );
  final Future<FirebaseApp> firebase = Firebase.initializeApp();
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: firebase,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
              appBar: AppBar(
                title: const Text("Error"),
              ),
              body: Center(
                child: Text("${snapshot.error}"),
              ),
            );
          } //กรอบ

          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              resizeToAvoidBottomInset: false,
              body: Container(
                width: double.infinity,
                height: double.infinity,
                decoration: const BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage('assets/images/background.jpg'),
                    fit: BoxFit.fitHeight,
                  ),
                ),
                child: Container(
                  margin: const EdgeInsets.fromLTRB(50, 300, 50, 100),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(16),
                      gradient: const LinearGradient(colors: [
                        Colors.green,
                        Color.fromARGB(255, 208, 255, 0),
                      ])),
                  padding: const EdgeInsets.all(10),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(10, 2, 10, 0),
                    child: Form(
                      key: formKey,
                      child: SingleChildScrollView(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //กรอกอีเมลล์
                            Container(
                                padding: const EdgeInsets.all(10.0),
                                margin: const EdgeInsets.only(top: 12),
                                decoration: BoxDecoration(
                                    color: Colors.green[50],
                                    borderRadius: BorderRadius.circular(16)),
                                child: TextField(
                                    decoration: const InputDecoration.collapsed(
                                        hintText: "Email"),
                                    keyboardType: TextInputType.emailAddress,
                                    onChanged: (String? email) {
                                      profile.email = email!;
                                      MultiValidator([
                                        RequiredValidator(
                                            errorText: "กรุณากรอกอีเมล"),
                                        EmailValidator(
                                            errorText: "รูปแบบอีเมลไม่ถูกต้อง")
                                      ]);
                                    })),
                            const SizedBox(
                              height: 5,
                            ),
                            //กรอกพาสเวิร์ด
                            Container(
                                padding: const EdgeInsets.all(10.0),
                                margin: const EdgeInsets.only(top: 12),
                                decoration: BoxDecoration(
                                    color: Colors.green[50],
                                    borderRadius: BorderRadius.circular(16)),
                                child: TextField(
                                    decoration: const InputDecoration.collapsed(
                                        hintText: "password"),
                                    obscureText: true,
                                    onChanged: (String? password) {
                                      profile.password = password!;
                                      Validator:
                                      RequiredValidator(
                                          errorText: "กรุณากรอกพาสเวิร์ด");
                                    })),
                            //ปุุ่มลงชื่อเข้าใช้
                            SizedBox(
                                width: double.infinity,
                                child: ElevatedButton(
                                  onPressed: () async {
                                    if (formKey.currentState!.validate()) {
                                      formKey.currentState?.save();
                                      try {
                                        await FirebaseAuth.instance
                                            .signInWithEmailAndPassword(
                                                email: profile.email,
                                                password: profile.password)
                                            .then((value) {
                                          formKey.currentState?.reset();
                                          Navigator.pushReplacement(context,
                                              MaterialPageRoute(
                                                  builder: (context) {
                                            return home();
                                          }));
                                        });
                                      } on FirebaseAuthException catch (e) {
                                        Fluttertoast.showToast(
                                            msg: '${e.message}',
                                            gravity: ToastGravity.CENTER);
                                      }
                                    }
                                  },
                                  style: ElevatedButton.styleFrom(
                                      backgroundColor: const Color.fromARGB(
                                          255, 2, 136, 13)),
                                  child: const Text("ลงชื่อเข้าใช้"),
                                )),
                            //คั่น don't
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(children: <Widget>[
                                Expanded(
                                    child: Divider(color: Colors.green[800])),
                                const Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Text(
                                    "Don't have an account?",
                                    style: TextStyle(color: Colors.black87),
                                  ),
                                ),
                                Expanded(
                                    child: Divider(
                                  color: Colors.green[800],
                                ))
                              ]),
                            ),
                            //ปุ่มสร้างบัญชีผู้ใช้
                            SizedBox(
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return const Register();
                                  }));
                                },
                                style: ElevatedButton.styleFrom(
                                    backgroundColor:
                                        const Color.fromARGB(255, 2, 136, 13)),
                                child: const Text("สร้างบัญชีผู้ใช้"),
                              ),
                            ),
                            //คั่นระหว่าง สมัครสมาชิกหรือลืมรหัสผ่าน
                            Container(
                              margin: const EdgeInsets.only(top: 5),
                              child: Row(children: <Widget>[
                                Expanded(
                                    child: Divider(color: Colors.green[800])),
                                const Padding(
                                  padding: EdgeInsets.all(6),
                                  child: Text(
                                    "other",
                                    style: TextStyle(color: Colors.black87),
                                  ),
                                ),
                                Expanded(
                                    child: Divider(
                                  color: Colors.green[800],
                                ))
                              ]),
                            ),
                            // ลืมรหัสผ่าน
                            SizedBox(
                              width: double.infinity,
                              child: ElevatedButton(
                                onPressed: () {
                                  Navigator.pushReplacement(context,
                                      MaterialPageRoute(builder: (context) {
                                    return const ResetPasswordPage();
                                  }));
                                },
                                style: ElevatedButton.styleFrom(
                                    backgroundColor:
                                        const Color.fromARGB(255, 2, 136, 13)),
                                child: const Text("ลืมรหัสผ่าน"),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            );
          }
          return const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
        });
  }
}
